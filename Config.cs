﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMicrosoftGraphData
{
    internal struct Config
    {
        public string ClientID { get; set; }
        public string TenantID { get; set; }
    }
}
