﻿extern alias BetaLib;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;
using Microsoft.Graph;
using Beta = BetaLib.Microsoft.Graph;
using System.Net.Http.Headers;

namespace GetMicrosoftGraphData
{
    class Program : IAuthenticationProvider
    {
        private static string[] scopes = new string[] { "https:/graph.microsoft.com/beta/.default" };
        static async Task Main(string[] args)
        {
            try
            {
                Console.WriteLine(".NET Core Graph Tutorial\n");

                var appId = "ee93a5fd-c0fd-4b5f-910d-c0204e861486";
                /*"64e10837 -3fd6-461d-acf4-f9b06fe2a23e";*/
                var scopes = new string[] { "User.Read", "Calendars.Read", "Presence.Read.All" };

                // Initialize the auth provider with values from appsettings.json
                var authProvider = new DeviceCodeAuthProvider(appId, scopes);

                // Request a token to sign in the user
                var accessToken = authProvider.GetAccessToken().Result;

                // Initialize Graph client
                GraphHelper.Initialize(authProvider);

                // Get signed in user
                var user = GraphHelper.GetMeAsync().Result;
                Console.WriteLine($"Welcome {user.DisplayName}!\n");
                int choice = -1;

                while (choice != 0)
                {
                    Console.WriteLine("Please choose one of the following options:");
                    Console.WriteLine("0. Exit");
                    Console.WriteLine("1. Display access token");
                    Console.WriteLine("2. List single user presence");
                    Console.WriteLine("3. List multiple users' presence");

                    try
                    {
                        choice = int.Parse(Console.ReadLine());
                    }
                    catch (System.FormatException)
                    {
                        // Set to invalid value
                        choice = -1;
                    }

                    switch (choice)
                    {
                        case 0:
                            // Exit the program
                            Console.WriteLine("Goodbye...");
                            break;
                        case 1:
                            // Display access token
                            Console.WriteLine($"Access token: {accessToken}\n");
                            break;
                        case 2:
                            //ListCalendarEvents();
                            var presence = await GraphHelper.GetPresenceAsync();
                            Console.WriteLine($"User with id: {presence.Id}\n Availability: {presence.Availability}\n Activity: {presence.Activity}");
                            break;
                        case 3:
                            var presences = await GraphHelper.GetPresencesAsync();
                            foreach (var p in presences)
                            {
                                Console.WriteLine($"User with id: {p.Id}\n Availability: {p.Availability}\n Activity: {p.Activity}\n");
                            }
                            break;
                        default:
                            Console.WriteLine("Invalid choice! Please try again.");
                            break;
                    }
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.WriteLine(e.Message + (String.IsNullOrEmpty(e.InnerException.Message) ? "" : e.InnerException.Message)); ;
            }
        }
        

        static async Task<string> SaveToken()
        {
            var tokne = await GetAccessToken();
            return tokne;

        }
        public async Task AuthenticateRequestAsync(HttpRequestMessage requestMessage)
        {
            requestMessage.Headers.Authorization =
                new AuthenticationHeaderValue("bearer", await GetAccessToken());
        }

        static AuthorizationCodeProvider GetAuthProvider()
        {
            //authenticate
            IConfidentialClientApplication app;
            Console.WriteLine("Initializing MSAL.NET");
            app = ConfidentialClientApplicationBuilder
                .Create("ee93a5fd-c0fd-4b5f-910d-c0204e861486")
                .WithClientSecret("19_N1Un997HoBWw.-~Dc~-i1~yUekPUs_o")
                .WithAuthority(AadAuthorityAudience.AzureAdAndPersonalMicrosoftAccount, true)
                .Build();


            ////acquire token
            //AuthenticationResult result = null;
            ////try
            ////{
            //Console.WriteLine("Acquiring Token");
            //result = await app
            //        .AcquireTokenForClient(scopes)
            //        .ExecuteAsync();
            //Console.ForegroundColor = ConsoleColor.Green;
            //Console.WriteLine("Token Acquired");
            //Console.ResetColor();

            AuthorizationCodeProvider authProvider = new AuthorizationCodeProvider(app, scopes);
            return authProvider;
        }

        static async Task<string> GetAccessToken()
        {
            var authProvider = GetAuthProvider();
            var result = await authProvider.ClientApplication.AcquireTokenForClient(scopes).ExecuteAsync();
            return result.AccessToken;
        }

        static async Task<Beta.Presence> AsyncGET()
        {
            var authProvider = GetAuthProvider();
            // Create an authentication provider by passing in a client application and graph scopes.
            //DeviceCodeProvider authProvider = new DeviceCodeProvider(app, scopes/*publicClientApplication/*, graphScopes*/);
            // Create a new instance of GraphServiceClient with the authentication provider.
            Beta.GraphServiceClient graphClient = new Beta.GraphServiceClient(authProvider);
            //graphClient.BaseUrl = "https://graph.microsoft.com/beta";
            //var request = graphClient.Me.Presence.Request().GetHttpRequestMessage().ToString();
            //Console.WriteLine(request);

          
            var presence = await graphClient.Me.Presence
                .Request()
                .GetAsync();

           return presence;

        }

        static void Get()
        {
            // Build a client application.
            IPublicClientApplication publicClientApplication = PublicClientApplicationBuilder
                        .Create("7c4dc9c5-f439-4dd4-aaba-1db05a0dbe8d") //client id
                        .Build();
            // Create an authentication provider by passing in a client application and graph scopes.
            DeviceCodeProvider authProvider = new DeviceCodeProvider(publicClientApplication/*, graphScopes*/);
            // Create a new instance of GraphServiceClient with the authentication provider.
            GraphServiceClient graphClient = new GraphServiceClient(authProvider);
            graphClient.BaseUrl = "https://graph.microsoft.com/beta";
            var request = graphClient.Me.Messages.Request().GetAsync();
            
        }
    }
}
