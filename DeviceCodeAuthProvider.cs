﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GetMicrosoftGraphData
{
    public class DeviceCodeAuthProvider : IAuthenticationProvider
    {
        private IPublicClientApplication _msalClient;
        private string[] _scopes;
        private IAccount _userAccount;

        public DeviceCodeAuthProvider(string appId, string[] scopes)
        {
            _scopes = scopes;
            string authority = "https://login.microsoftonline.com/64e10837-3fd6-461d-acf4-f9b06fe2a23e";
            _msalClient =
                PublicClientApplicationBuilder
                .Create(appId)
                .WithAuthority(authority/*AadAuthorityAudience.AzureAdAndPersonalMicrosoftAccount, true*/)
                .Build();
        }

        public async Task<string> GetAccessToken()
        {
            // If there is no saved user account, the user must sign-in
            if (_userAccount == null)
            {
                try
                {
                    // Invoke device code flow so user can sign-in with a browser
                    var result = await _msalClient.AcquireTokenWithDeviceCode(_scopes,
                        deviceCodeResult =>
                        {
                            // This will print the message on the console which tells the user where to go sign-in using 
                            // a separate browser and the code to enter once they sign in.
                            // The AcquireTokenWithDeviceCode() method will poll the server after firing this
                            // device code callback to look for the successful login of the user via that browser.
                            // This background polling (whose interval and timeout data is also provided as fields in the 
                            // deviceCodeCallback class) will occur until:
                            // * The user has successfully logged in via browser and entered the proper code
                            // * The timeout specified by the server for the lifetime of this code (typically ~15 minutes) has been reached
                            // * The developing application calls the Cancel() method on a CancellationToken sent into the method.
                            //   If this occurs, an OperationCanceledException will be thrown (see catch below for more details).
                            Console.WriteLine(deviceCodeResult.Message);
                            return Task.FromResult(0);
                        }).ExecuteAsync(); 

                    _userAccount = result.Account;
                    return result.AccessToken;
                }
                catch (Exception exception)
                {
                    Console.WriteLine($"Error getting access token: {exception.Message}");
                    return null;
                }
            }
            else
            {
                // If there is an account, call AcquireTokenSilent
                // By doing this, MSAL will refresh the token automatically if
                // it is expired. Otherwise it returns the cached token.

                var result = await _msalClient
                    .AcquireTokenSilent(_scopes, _userAccount)
                    .ExecuteAsync();

                return result.AccessToken;
            }
        }

        // This is the required function to implement IAuthenticationProvider
        // The Graph SDK will call this function each time it makes a Graph
        // call.
        public async Task AuthenticateRequestAsync(HttpRequestMessage requestMessage)
        {
            requestMessage.Headers.Authorization =
                new AuthenticationHeaderValue("bearer", await GetAccessToken());
        }
    }
}