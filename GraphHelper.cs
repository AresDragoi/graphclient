﻿extern alias BetaLib;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Beta = BetaLib.Microsoft.Graph;

namespace GetMicrosoftGraphData
{
    public class GraphHelper
    {
        private static Beta.GraphServiceClient graphClient;
        public static void Initialize(IAuthenticationProvider authProvider)
        {
            graphClient = new Beta.GraphServiceClient(authProvider);
        }

        /// <summary>
        /// Gets presence information
        /// </summary>
        /// <returns>Beta.Presence</returns>
        internal static async Task<Beta.Presence> GetPresenceAsync()
        {
            //var authProvider = GetAuthProvider();
            // Create an authentication provider by passing in a client application and graph scopes.
            //DeviceCodeProvider authProvider = new DeviceCodeProvider(app, scopes/*publicClientApplication/*, graphScopes*/);
            // Create a new instance of GraphServiceClient with the authentication provider.
            //Beta.GraphServiceClient graphClient = new Beta.GraphServiceClient(authProvider);
            //graphClient.BaseUrl = "https://graph.microsoft.com/beta";
            //var request = graphClient.Me.Presence.Request().GetHttpRequestMessage().ToString();
            //Console.WriteLine(request);


            //send request for single presence
            var presence = await graphClient.Me.Presence
                .Request()
                .GetAsync();


            return presence;

        }

        internal static async Task<Beta.ICloudCommunicationsGetPresencesByUserIdCollectionPage> GetPresencesAsync()
        {

            var ids = new List<String>()
                            {
                                "73a08044-cc8c-4f54-a736-c30e30efbfc5",
                                "62355e49-658b-44d3-a9b6-1705fd19b8b9"
                                //"fa8bf3dc-eca7-46b7-bad1-db199b62afc3",
                                //"66825e03-7ef5-42da-9069-724602c31f6b"
                            };
            var presences = await graphClient.Communications
                .GetPresencesByUserId(ids)
                .Request()
                .PostAsync();

            return presences;
        }

        internal static async Task<Beta.User> GetMeAsync()

        {
            try
            {
                // GET /me
                return await graphClient.Me.Request().GetAsync();
            }
            catch (ServiceException ex)
            {
                Console.WriteLine($"Error getting signed-in user: {ex.Message}");
                return null;
            }
        }

        //public static async Task<IEnumerable<Event>> GetEventsAsync()
        //{
        //    try
        //    {
        //        // GET /me/events
        //        var resultPage = await graphClient.Me.Events.Request()
        //            // Only return the fields used by the application
        //            .Select(e => new
        //            {
        //                e.Subject,
        //                e.Organizer,
        //                e.Start,
        //                e.End
        //            })
        //            // Sort results by when they were created, newest first
        //            .OrderBy("createdDateTime DESC")
        //            .GetAsync();

        //        return resultPage.CurrentPage;
        //    }
        //    catch (ServiceException ex)
        //    {
        //        Console.WriteLine($"Error getting events: {ex.Message}");
        //        return null;
        //    }
        //}

        public static async Task<IEnumerable<Event>> GetPresences()
        {
            try
            {
                // GET /me/events
                var ids = new List<String>()
                            {
                                "fa8bf3dc-eca7-46b7-bad1-db199b62afc3",
                                "66825e03-7ef5-42da-9069-724602c31f6b"
                            };

                return null;
                //await graphClient.Communications
                //          .GetPresencesByUserId(ids)
                //          .Request()
                //          .PostAsync();
            }
            catch (ServiceException ex)
            {
                Console.WriteLine($"Error getting events: {ex.Message}");
                return null;
            }
        }
    }
}